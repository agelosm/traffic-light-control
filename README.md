In order to run the application with IntelliJ:

1. Clone the repository using IntelliJ
2. Select Project Structure->Libraries->'+' and select **jade.jar** and **javafx-sdk-..../lib**
3. Select Run->Edit Configurations->Modify Options->Add VM options and add:
--module-path <path_to>/javafx-sdk-..../lib --add-modules=javafx.controls,javafx.fxml
4. Build and run Project
