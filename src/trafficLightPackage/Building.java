package trafficLightPackage;

import java.util.ArrayList;

enum BuildingType{

    HOSPITAL,
    FIREHOUSE,
    POLICE_STATION,
    HOUSING
}

public class Building {

    ArrayList<Node> nodes;
    BuildingType type = BuildingType.HOUSING;

    // A building is represented by four nodes in the graph
    Building(ArrayList<Node> nds){
        nodes = nds;
    }

    Building(ArrayList<Node> nds, BuildingType tp){
        nodes = nds;
        type = tp;
    }
}
