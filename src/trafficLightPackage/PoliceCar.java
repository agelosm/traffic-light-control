package trafficLightPackage;

import jade.core.AID;

import java.util.ArrayList;

public class PoliceCar extends Car {
    /**
     *  A class that represents a police car, with it's corresponding emergency priority
     */

    protected void setup() {

        super.setup();
        init(dto.Cid, dto.path, dto.trafficLights, dto.place, dto.emergency);
    }

    protected void init(AID controller, ArrayList<Node> path, ArrayList<TrafficLight> trafficLights, Node place, boolean onEmergency) {
        super.init(controller, path, trafficLights, place, onEmergency);
        this.emergencyPriority = 3;
    }
}
