package trafficLightPackage;

import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;


public class TrafficLightRequest extends Action {
    /**
     * Ontology class
     *
     * @param senders               Message sender
     * @param row                   Row of the traffic light in the grid
     * @param column                Column of the traffic light in the grid
     * @param direction             Direction that the car wants to go to
     * @param color                 Color of the light
     * @param emergencyPriority     The emergency priority of the vehicle (this is double checked by the controller)
     */

    public TrafficLightRequest(ArrayList senders, int row, int column, ArrayList direction, ArrayList color, ArrayList emergencyPriority, ArrayList onEmergency) {
        this.senders = senders;
        this.row = row;
        this.column = column;
        this.emergencyPriority = emergencyPriority;
        this.onEmergency = onEmergency;
        this.direction = direction;
        this.color = color;
    }

    public TrafficLightRequest(){
        this.senders = new ArrayList();
        this.emergencyPriority = new ArrayList();
        this.onEmergency = new ArrayList();
        this.direction = new ArrayList();
        this.color = new ArrayList();
    }

    private ArrayList senders;
    private int row;
    private int column;
    private ArrayList emergencyPriority;
    private ArrayList onEmergency;
    private ArrayList direction;
    private ArrayList color;

    public ArrayList getDirection() {
        return direction;
    }

    public void setDirection(ArrayList direction) {
        this.direction = direction;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public ArrayList getEmergencyPriority() {
        return emergencyPriority;
    }

    public void setEmergencyPriority(ArrayList emergencyPriority) {
        this.emergencyPriority = emergencyPriority;
    }

    public ArrayList getOnEmergency() {
        return onEmergency;
    }

    public void setOnEmergency(ArrayList onEmergency) {
        this.onEmergency = onEmergency;
    }

    public ArrayList getSenders() {
        return senders;
    }

    public void setSenders(ArrayList senders) {
        this.senders = senders;
    }

    public ArrayList getColor() {
        return color;
    }

    public void setColor(ArrayList color) {
        this.color = color;
    }

    public ACLMessage toACLMessage(ContentManager contentManager, AID rid){

        contentManager.registerLanguage(new LEAPCodec());
        contentManager.registerOntology( TrafficLightRequestOntology.getInstance());

        Codec codec = new LEAPCodec();
        Ontology ontology = TrafficLightRequestOntology.getInstance();

        ACLMessage aclmsg = new ACLMessage( ACLMessage.INFORM );
        aclmsg.setLanguage(codec.getName());
        aclmsg.setOntology(ontology.getName());

        try {
            contentManager.fillContent(aclmsg, this);
            aclmsg.addReceiver(rid);

        } catch (OntologyException oe) {
            oe.printStackTrace();

        } catch (Codec.CodecException e) {
            e.printStackTrace();
        }
        return aclmsg;
    }

    static public TrafficLightRequest getMergedACLMessage( java.util.ArrayList<TrafficLightRequest> requests ){

        TrafficLightRequest request = new TrafficLightRequest();

        for( TrafficLightRequest trafficLightRequest : requests ){

            for( Object sender : trafficLightRequest.getSenders().toList() ){
                request.senders.add( (AID)sender );
            }
            for( Object dir : trafficLightRequest.getDirection().toList() ){
                request.direction.add( (String)dir );
            }
            for( Object col : trafficLightRequest.getColor().toList() ){

                request.color.add( col );
            }

            for( Object priority : trafficLightRequest.getEmergencyPriority().toList() ){

                request.emergencyPriority.add( (Integer) priority );
            }
            for( Object onEm : trafficLightRequest.getOnEmergency().toList() ){

                request.onEmergency.add( onEm );
            }
        }
        request.row = requests.get(0).row;
        request.column = requests.get(0).column;

        return request;
    }

}

