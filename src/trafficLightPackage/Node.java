package trafficLightPackage;

import java.util.*;

public class Node {
    /**
     * Node of the Graph class, used in path calculations
     */
    public boolean direction_values[];
    int row, col;

    public Node(String name, int row, int col, boolean[] direction_values) {
        this.row = row;
        this.col = col;
        this.direction_values = direction_values;
        this.name = name;

    }

    public boolean right(){ return direction_values[0]; }
    public boolean left(){ return direction_values[1]; }
    public boolean up(){ return  direction_values[2]; }
    public boolean down(){ return  direction_values[3]; }

    private String name;

    public ArrayList<Node> shortestPath = new ArrayList<>();

    private Integer distance = Integer.MAX_VALUE;

    Map<Node, Integer> adjacentNodes = new HashMap<>();

    public void addDestination(Node destination, int distance) {
        adjacentNodes.put(destination, distance);
    }

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(ArrayList<Node> shortestPath) {
        this.shortestPath = shortestPath;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Map<Node, Integer> getAdjacentNodes() {
        return adjacentNodes;
    }

    public void setAdjacentNodes(Map<Node, Integer> adjacentNodes) {
        this.adjacentNodes = adjacentNodes;
    }

    public boolean equals(Node o) {
        if ((o.row == this.row) && (o.col == this.col)) {
            return true;
        } else {
            return  false;
        }
    }
}