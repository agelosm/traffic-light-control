package trafficLightPackage;

public class Globals {

    public static int timestepInterval = 1000; // in milliseconds
    public static int trafficLightInterval = 7;
    public static int trafficLightEmergencyInterval = 4;
}
