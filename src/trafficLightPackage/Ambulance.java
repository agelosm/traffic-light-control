package trafficLightPackage;
import jade.core.AID;
import java.util.ArrayList;

public class Ambulance extends Car{
    /**
     *  A class that represents an ambulance, with it's corresponding emergency priority
     */

    protected void setup() {
        super.setup();
        init(dto.Cid, dto.path, dto.trafficLights, dto.place, dto.emergency);
    }

    void init(AID controller, ArrayList<Node> path, ArrayList<TrafficLight> trafficlights, Node place, boolean onemergency){
        super.init(controller, path, trafficlights, place, onemergency);
        this.emergencyPriority = 5;
    }
}
