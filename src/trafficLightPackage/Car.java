package trafficLightPackage;
import jade.core.AID;
import jade.core.Agent;
import java.util.ArrayList;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;


public class Car extends Agent{
    /**
     *  A class that represents a car-agent, and includes all of it's functionality
     *
     */

    ArrayList<Node> path;

    ArrayList<TrafficLight> trafficlights = new ArrayList<TrafficLight>();

    // The car's current place
    Node place;
    // states if the car is on emergency
    boolean onEmergency;
    // Priority weight. This filed is different for each inheriting class
    public int emergencyPriority;
    Direction direction, nextDirection;
    AID controller;

    CarDTO dto;
    WorldDrawer worldDrawer;
    int spawnTimestep;

    protected void setup( ){

        // Get the arguments passed on by the spawner
        Object[] args = getArguments();
        dto = (CarDTO) args[0];

        worldDrawer = dto.worldDrawer;
        spawnTimestep = worldDrawer.currentTimestep;

        // Add a ticking behaviour, repeating at each timestep
        Behaviour loop = new TickerBehaviour( this, Globals.timestepInterval )
        {
            protected void onTick() {

                // Terminate upon path completion
                if (path.size() == 0) {
                    doDelete();
                }

                // At each timestep after the first, just follow the path
                if( dto.worldDrawer.currentTimestep > spawnTimestep ) {
                    MoveForward();

                } else {
                    if( path.size() > 1 ) {
                        // At it's first timestep, agent is just drawn in place, looking towards the right direction
                        direction = Direction.getDirectionFrom2Nodes(path.get(0), path.get(1));
                    }
                }
                updateDraw();
            }
        };

        addBehaviour( loop );

    }

    @Override
    protected void takeDown() {
        super.takeDown();
        worldDrawer.removeCarPosition(getAID());

    }

    // Function to initialize all the local variables
    void init(AID controller, ArrayList<Node> p, ArrayList<TrafficLight> trafficlights, Node place, boolean onemergency){
        this.path = (ArrayList<Node>) p.clone();
        this.trafficlights = trafficlights;
        this.place = place;
        this.onEmergency = onemergency;
        this.controller = controller;

        if( path.isEmpty() ){
            doDelete();
            return;
        }

        dto.start = path.get(0);

        // If the car is not arrived yet, calculate it's direction
        if( path.size() > 1 ){
            direction = Direction.getDirectionFrom2Nodes(path.get(0), path.get(1));
            dto.end = path.get(path.size() - 1);
        } else {
            dto.end = path.get(0);
        }
        direction = Direction.getDirectionFrom2Nodes(path.get(0), path.get(1));
        updateDraw();
    }

    TrafficLight isTrafficLightOnNode( Node node ){

        for( TrafficLight trafficLight : trafficlights ) {
            if (node.equals(trafficLight.node)) {
                return trafficLight;
            }
        }
        return null;
    }


    Boolean isTrafficLightOpen( TrafficLight trafficLight, Direction dir){

        Boolean advance = true;

        // All emergency vehicles inform the controller about the traffic light they want to go through
        if(trafficLight.openDirections.contains(dir)){
            advance = true;
            SendMessage(dir, false, trafficLight.node);
        } else {
            advance = false;
            SendMessage(dir, true, trafficLight.node);
        }
        return advance;
    }

    void advance( ){

        if( path.size() > 0 ) {
            direction = Direction.getDirectionFrom2Nodes(place, path.get(0));
            place = path.get(0);
            path.remove(0);
        }
    }

    void updateDraw( ){

        dto.place = place;
        dto.setDirection(direction);
        dto.setNextDirection(nextDirection);
        worldDrawer.getCarPosition(getAID(), dto);
    }

    Direction getNextDirection( ){

        nextDirection = null;
        if( path.size() > 1 ){

            nextDirection = Direction.getDirectionFrom2Nodes(place, path.get(0));
        }
        return nextDirection;
    }

    void checkNextNodeForRedLight( ){
        nextDirection = getNextDirection();
        if( nextDirection != null ){

            TrafficLight nextTrafficLight = isTrafficLightOnNode(path.get(0));
            if( nextTrafficLight != null ){

                Direction doubleNextDirection = nextDirection;
                if( path.size() > 3 ){
                    doubleNextDirection = Direction.getDirectionFrom2Nodes(path.get(2), path.get(3));
                }
                isTrafficLightOpen(nextTrafficLight, doubleNextDirection );
            }
        }
    }

    void MoveForward(){

        TrafficLight trafficLight = isTrafficLightOnNode(place);
        if( trafficLight != null ){

            if( isTrafficLightOpen(trafficLight, nextDirection) ){

               advance();
               checkNextNodeForRedLight();
            }
        } else {
            advance();
            checkNextNodeForRedLight();
        }
    }

    // Initialize a JADE list that will be used for a message
    jade.util.leap.ArrayList initList( Object o ){

        jade.util.leap.ArrayList list = new jade.util.leap.ArrayList();
        list.add(o);
        return list;
    }

    /*
        This function handles all car agent's messages to the traffic light controller
     */
    void SendMessage(Direction direction, boolean trafficLightColor, Node lightNode){

        Direction lightDirection;
        if( path.size() > 1 ) {
         lightDirection = Direction.getDirectionFrom2Nodes(path.get(0), path.get(1));
        } else {
            lightDirection = Direction.getDirectionFrom2Nodes(place, path.get(0));
        }

        try {

            // Create the message using the TrafficLightRequest ontology
            TrafficLightRequest trafficLightRequest = new TrafficLightRequest(initList(getAID()),
                    lightNode.row, lightNode.col, initList(lightDirection.toString()), initList(trafficLightColor), initList(emergencyPriority), initList(onEmergency));

            send(trafficLightRequest.toACLMessage(getContentManager(), dto.Cid));
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

}
