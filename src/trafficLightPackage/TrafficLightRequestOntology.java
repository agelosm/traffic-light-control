package trafficLightPackage;

import jade.content.onto.Ontology;
import jade.content.onto.*;
import jade.content.schema.*;


public class TrafficLightRequestOntology extends Ontology {
    /**
     * The ontology schema
     */

    public static final String ONTOLOGY_NAME = "Traffic-light-request-ontology";

    // VOCABULARY
    public static final String TRAFFIC_LIGHT_REQUEST = "Traffic Light Request";
    public static final String SENDERS = "Senders";

    public static final String ROW = "Row";
    public static final String COLUMN = "Column";

    public static final String DIRECTION = "Direction";
    public static final String COLOR = "Color";

    public static final String EMERGENCY_PRIORITY = "EmergencyPriority";
    public static final String ON_EMERGENCY = "OnEmergency";

    // The singleton instance of this ontology
    private static Ontology theInstance = new TrafficLightRequestOntology();

    // This is the method to access the singleton music shop ontology object
    public static Ontology getInstance() {
        return theInstance;
    }

    private TrafficLightRequestOntology() {

        super(ONTOLOGY_NAME, BasicOntology.getInstance());
        try {
            AgentActionSchema cs;
            add( cs = new AgentActionSchema(TRAFFIC_LIGHT_REQUEST), TrafficLightRequest.class);

            cs.add(SENDERS, (ConceptSchema) getSchema(BasicOntology.AID), 1, ObjectSchema.UNLIMITED);
            cs.add(ROW, (PrimitiveSchema) getSchema(BasicOntology.INTEGER));
            cs.add(COLUMN, (PrimitiveSchema) getSchema(BasicOntology.INTEGER));
            cs.add(DIRECTION, (PrimitiveSchema) getSchema(BasicOntology.STRING), 1, ObjectSchema.UNLIMITED);
            cs.add(COLOR, (PrimitiveSchema) getSchema(BasicOntology.BOOLEAN), 1, ObjectSchema.UNLIMITED);
            cs.add(EMERGENCY_PRIORITY, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), 1, ObjectSchema.UNLIMITED);
            cs.add(ON_EMERGENCY, (PrimitiveSchema) getSchema(BasicOntology.BOOLEAN), 1, ObjectSchema.UNLIMITED);

        }
        catch (OntologyException oe) {
            oe.printStackTrace();
        }
    }
}