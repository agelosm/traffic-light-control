package trafficLightPackage;

import jade.core.AID;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import java.util.ArrayList;
import java.util.HashMap;


public class TrafficLightWorld {
    /**
     * This class is spawned via the GUI-thread, and handles the generation of the controller agent,
     * as well as the car agents
     */

    AID Cid;

    ArrayList<Car> cars = new ArrayList<>();
    HashMap<AID, Car> aidCarHashMap = new HashMap<>();
    City city;
    ArrayList<TrafficLight> trafficLights;

    Runtime rt = Runtime.instance();
    ProfileImpl p = new ProfileImpl(false);
    AgentContainer container = rt.createMainContainer(p);
    AgentController Agent = null;

    ArrayList<AgentController> controllers = new ArrayList<>();

    int time = 0;
    int example = 1;

    Main.Grid guiGrid;
    private WorldDrawer worldDrawer;

    public TrafficLightWorld(Main.Grid guidGrid, WorldDrawer worldDrawer) throws StaleProxyException {

        this.guiGrid = guidGrid;
        this.worldDrawer = worldDrawer;
    }

    void addCar(Car car) {
        cars.add(car);
        aidCarHashMap.put(car.getAID(), car);
    }

    void constructWorld() throws StaleProxyException {

        city = new City(guiGrid, container, worldDrawer);

        Object[] args = {city.trafficLights, worldDrawer};

        Agent = container.createNewAgent("TrafficController", "trafficLightPackage.TrafficController", args);
        controllers.add(Agent);

        Cid = new AID("TrafficController", false);

        city.setCid( Cid );

        Agent.start();
    }

    void createStochasticCar( ) throws StaleProxyException {

        double p = Math.random();

        /* Create a car with p=0.6 */
        if( p > 0.4 ){

            /* It's a private car with p = 0.56, and an emergency vehicle with p=0.02 each */
            if( p < 0.94 ){
                Node houseNode = city.getRandomHouse();
                createPrivateCar( houseNode, city.getRandomHouse(houseNode) );
            } else if( p < 0.96 ){
                createPoliceCar(city.getFirstPoliceStation(), city.getRandomHouse());
            } else if( p < 0.98 ) {
                createAmbulance(city.getFirstHospital(), city.getRandomHouse());
            }else {
                createFiretruck(city.getFirstFirehouse(), city.getRandomHouse());
            }
        }
    }

    void runTimestep() throws StaleProxyException {

        if( example == 1 ) {
            if (time == 1) {
                createAmbulance(city.grid[6][4], city.getFirstFirehouse());
                createPrivateCar(city.grid[11][9], city.getFirstPoliceStation());
            }
        } else {
            if( worldDrawer.statisticsManager.currentAgents < 20 ) {
                createStochasticCar();
            }
        }
        for( TrafficLight trafficLight : city.trafficLights ){

            trafficLight.update();
        }

        time++;
    }

    public void createPoliceCar( Node from, Node to ) throws StaleProxyException {
        createCar("police", "trafficLightPackage.PoliceCar", from, to);
        worldDrawer.statisticsManager.totalPoliceCars++;
    }

    public void createPrivateCar( Node from, Node to ) throws StaleProxyException {
        createCar("private", "trafficLightPackage.PrivateCar", from, to);
        worldDrawer.statisticsManager.totalPrivateCars++;
    }

    public void createAmbulance( Node from, Node to ) throws StaleProxyException {
        createCar("ambulance", "trafficLightPackage.Ambulance", from, to);
        worldDrawer.statisticsManager.totalAmbulances++;
    }

    public void createFiretruck( Node from, Node to ) throws StaleProxyException {
        createCar("firetruck", "trafficLightPackage.FireTruck", from, to);
        worldDrawer.statisticsManager.totalFiretrucks++;
    }

    public void createCar( String name, String className, Node from, Node to) throws StaleProxyException {
        city.BellmanFord(to);
        ArrayList<Node> path = (ArrayList<Node>) from.shortestPath.clone();
        CarDTO dto = new CarDTO(Cid, path, city.trafficLights, from, true, worldDrawer);
        Object[] args2 = {dto};
        Agent = container.createNewAgent(name + time, className, args2);
        Agent.start();
    }

    public void destroy() {

        for( TrafficLight trafficLight : city.trafficLights ){

            trafficLight.terminate();
        }

        for( AgentController agentController : controllers ){
            try {
                agentController.kill();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
    }
}
