package trafficLightPackage;
import jade.core.AID;
import jade.wrapper.AgentContainer;
import jade.wrapper.StaleProxyException;
import java.util.ArrayList;
import java.util.Collections;

public class City extends Graph{
    /**
     *  This class represents the whole city
     *  It extends the graph class, in order to integrate path finding functionality using Bellman-Ford algorithm
     */

    int rows, columns;

    ArrayList<Building> buildings = new ArrayList<>();

    ArrayList<TrafficLight> trafficLights = new ArrayList<>();

    Node[][] grid;

    AgentContainer container;

    WorldDrawer worldDrawer;

    AID cid;

    public City(Main.Grid guiGrid, AgentContainer container, WorldDrawer worldDrawer){

        // Transfer the grid of the designer, into the internal architecture grid
        this.rows = guiGrid.getRows() + 1;
        this.columns = guiGrid.getColumns() + 1;
        this.grid = new Node[rows][columns];

        this.container = container;
        this.worldDrawer = worldDrawer;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                // Translate the edges of the designer, to edges of the internal representation of the city graph
                boolean direction_values[] = new boolean[]{true,true,true,true};
                //left
                if( i == 0 || !guiGrid.hasRoadLeft(i, j) ){
                    direction_values[1] = false;
                }
                //right
                if( i == rows-1 || !guiGrid.hasRoadRight(i, j) ){
                    direction_values[0] = false;
                }
                // down
                if( j == 0 || !guiGrid.hasRoadDown(i, j) ){
                    direction_values[3] = false;
                }
                // up
                if( j == columns-1 || !guiGrid.hasRoadUp(i,j) ){
                    direction_values[2] = false;
                }
                grid[i][j] = new Node(i+":"+j, i, j, direction_values);

                // If the node connects to at least one grid, add for the path calculations
                if( direction_values[0] || direction_values[1] || direction_values[2] || direction_values[3] ) {
                    addNode(grid[i][j]);
                }
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                // Connect  the graph edges, for path calculations
                if( grid[i][j].right() ){
                    grid[i][j].addDestination(grid[i][j+1], 1);
                }
                if( grid[i][j].left() ){
                    grid[i][j].addDestination(grid[i][j-1], 1);
                }
                if( grid[i][j].up() ){
                    grid[i][j].addDestination(grid[i-1][j], 1);
                }
                if( grid[i][j].down() ){
                    grid[i][j].addDestination(grid[i+1][j], 1);
                }

                // Check if the node forms a junction
                int count = 0;
                for (boolean var : grid[i][j].direction_values) {
                    count += (var ? 1 : 0);
                }

                // More than two roads colliding, need a traffic light
                if( count > 2 ){
                    try {
                        trafficLights.add(new TrafficLight(worldDrawer, cid, LightColor.RED, grid[i][j], i ,j, container));
                    } catch (StaleProxyException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // Construct the buildings
        for( int i = 0; i < guiGrid.getRows(); i++ ){
            for (int j = 0; j < guiGrid.getColumns(); j++) {

                ArrayList<Node> nodes = new ArrayList<>();

                // Each building corresponds to 4 nodes in the graph
                nodes.add(grid[i][j]);
                nodes.add(grid[i+1][j]);
                nodes.add(grid[i+1][j+1]);
                nodes.add(grid[i][j+1]);

                DrawMode drawMode = guiGrid.getCell(i,j).cellDrawMode;

                if( drawMode == DrawMode.DRAW_HOUSE){

                    buildings.add(new Building( nodes, BuildingType.HOUSING));

                } else if( drawMode == DrawMode.DRAW_HOSPITAL) {

                    buildings.add(new Building( nodes, BuildingType.HOSPITAL));

                } else if( drawMode == DrawMode.DRAW_POLICESTATION) {

                    buildings.add(new Building( nodes, BuildingType.POLICE_STATION));

                } else if( drawMode == DrawMode.DRAW_FIREHOUSE) {

                    buildings.add(new Building( nodes, BuildingType.FIREHOUSE));
                }
            }
        }
    }

    private Node getFirstBuildingOfType( BuildingType type ){

        for( Building building : buildings ){

            if( building.type == type ){

                for( Node n : building.nodes ){

                    if( n.left() || n.right() || n.down() || n.up() ){
                        return n;
                    }
                }
            }
        }
        return null;
    }

    public Node getFirstHospital( ){

        return getFirstBuildingOfType(BuildingType.HOSPITAL);
    }

    public Node getFirstPoliceStation( ){

        return getFirstBuildingOfType(BuildingType.POLICE_STATION);
    }

    public Node getFirstFirehouse( ){

        return getFirstBuildingOfType(BuildingType.FIREHOUSE);
    }

    public Node getRandomHouse( ){

        ArrayList<Building> shuffledBuildings = (ArrayList<Building>) buildings.clone();
        Collections.shuffle(shuffledBuildings);

        for( Building building : shuffledBuildings ){

            if( building.type == BuildingType.HOUSING ){

                for( Node n : building.nodes ){

                    if( n.left() || n.right() || n.down() || n.up() ){
                        return n;
                    }
                }
            }
        }
        return null;
    }

    // Same as above, but it returns a house different than the one at argument
    public Node getRandomHouse( Node differentThan ){

        ArrayList<Building> shuffledBuildings = (ArrayList<Building>) buildings.clone();
        Collections.shuffle(shuffledBuildings);

        for( Building building : shuffledBuildings ){

            if( building.type == BuildingType.HOUSING && !building.nodes.contains(differentThan) ){

                for( Node n : building.nodes ){

                    if( n.left() || n.right() || n.down() || n.up() ){
                        return n;
                    }
                }
            }
        }
        return null;
    }

    public void setCid(AID cid) {

        this.cid = cid;
    }
}
