package trafficLightPackage;

public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT;

        static Direction getDirectionFrom2Nodes( Node from, Node to ){

            Direction direction = UP;

            if (from.col < to.col) {
                direction = Direction.RIGHT;
            } else if (from.col > to.col) {
                direction = Direction.LEFT;
            } else if (from.row < to.row) {
                direction = Direction.DOWN;
            } else if (from.col > to.col) {
                direction = Direction.UP;
            }

            return direction;
        }
};
