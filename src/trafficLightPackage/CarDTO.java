package trafficLightPackage;
import jade.core.AID;
import java.util.ArrayList;

// A Data Transfer Object, that carries the information, passed on to a car agent, upon it's creation
public class CarDTO {

    AID Cid;
    ArrayList<Node> path;
    ArrayList<TrafficLight> trafficLights;
    Node place, start, end;
    boolean emergency;
    WorldDrawer worldDrawer;
    Direction direction, nextDirection;

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getNextDirection() {
        return nextDirection;
    }

    public void setNextDirection(Direction nextDirection) {
        this.nextDirection = nextDirection;
    }

    public CarDTO(AID cid, ArrayList<Node> path, ArrayList<TrafficLight> trafficLights, Node place, boolean emergency, WorldDrawer worldDrawer) {
        Cid = cid;
        this.path = path;
        this.trafficLights = trafficLights;
        this.place = place;
        this.emergency = emergency;
        this.worldDrawer = worldDrawer;
    }

}
