package trafficLightPackage;

import jade.wrapper.StaleProxyException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.Button;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main extends Application {
    /**
     * This is the main JavaFX thread.
     * It creates the City Designer, and spawns the TrafficLightWorld thread, when the user
     * selects to start the simulation
     */

    boolean showHoverCursor = true;

    DrawMode drawMode = DrawMode.DRAW_ROAD;

    int rows = 15;
    int columns = 20;
    double width = 1000;
    double height = 800;
    double grid_width = 800;
    double grid_height = 600;
    Grid grid = new Grid(columns, rows, grid_width, grid_height);;

    public int currentTimestep;

    Stage primStage;
    private WorldDrawer worldDrawer;
    private TrafficLightWorld world;
    private Thread thread;

    public boolean isPaused;

    String getStrFromDrawMode( DrawMode type ){

        switch (type){
            case DRAW_POLICESTATION:
                return "Police Station";
            case DRAW_HOUSE:
                return "House";
            case DRAW_HOSPITAL:
                return "Hospital";
            case DRAW_FIREHOUSE:
                return "Firehouse";
            case DRAW_ROAD:
                return "Road";
            default:
                return "House";
        }
    }

    void createButton(DrawMode mode, int startingY, ArrayList<Button> buttons) {

        if( mode != DrawMode.DRAW_NOTHING ) {
            Button button = new Button();
            button.setText("Add " + getStrFromDrawMode(mode));
            button.setTranslateX(420);
            button.setTranslateY(startingY + buttons.size() * 30);

            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    drawMode = mode;
                }
            });
            buttons.add(button);
        }
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            StackPane root = new StackPane();

            primStage = primaryStage;

            MouseGestures mg = new MouseGestures();

            int startingY = -300;

            Text label = new Text("City Designer");
            label.setTranslateX(405);
            label.setTranslateY(startingY - 2 * 30);
            label.setFont(Font.font ("Verdana", 25));


            ArrayList<Button> buttons = new ArrayList<>();
            for( DrawMode mode : DrawMode.values() ){
                createButton(mode, startingY, buttons);
            }

            Button saveButton = new Button();
            saveButton.setText("Save to file");
            saveButton.setTranslateX(420);
            saveButton.setTranslateY(startingY + buttons.size() * 30);

            saveButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        grid.saveToFile();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            });
            buttons.add(saveButton);

            Button loadButton = new Button();
            loadButton.setText("Load from file");
            loadButton.setTranslateX(420);
            loadButton.setTranslateY(startingY + buttons.size() * 30);
            buttons.add(loadButton);

            loadButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        loadFromFile();
                    } catch (IOException | StaleProxyException ioException) {
                        ioException.printStackTrace();
                    }
                }
            });

            Button startButton = new Button();
            startButton.setText("Start simulation");
            startButton.setTranslateX(420);
            startButton.setTranslateY(startingY + buttons.size() * 30);

            startButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        startWorldScene();
                    } catch (StaleProxyException | InterruptedException staleProxyException) {
                        staleProxyException.printStackTrace();
                    }
                }
            });
            buttons.add(startButton);

            Button startButtonExample = new Button();
            startButtonExample.setText("Start Example");
            startButtonExample.setTranslateX(420);
            startButtonExample.setTranslateY(startingY + buttons.size() * 30);

            startButtonExample.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        startWorldSceneExample();
                    } catch (StaleProxyException | InterruptedException staleProxyException) {
                        staleProxyException.printStackTrace();
                    }
                }
            });
            //buttons.add(startButtonExample);

            root.getChildren().add(grid);
            root.getChildren().add(label);
            for( Button b : buttons ){
                root.getChildren().add(b);
            }

            // fill grid
            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {

                    Cell cell = new Cell(grid, column, row);

                    mg.makePaintable(cell);

                    grid.add(cell, column, row);
                }
            }



            // create scene and stage
            Scene scene = new Scene(root, width, height);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void loadFromFile( ) throws IOException, StaleProxyException {

        try {

            FileChooser fileChooser = new FileChooser();
            File myObj = fileChooser.showOpenDialog(primStage);

            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] splited = data.split("\\s+");

                int row = Integer.parseInt(splited[0]);
                int col = Integer.parseInt(splited[1]);
                DrawMode dm = DrawMode.valueOf(splited[2]);
                boolean d1 = Boolean.parseBoolean(splited[3]);
                boolean d2 = Boolean.parseBoolean(splited[4]);
                boolean d3 = Boolean.parseBoolean(splited[5]);
                boolean d4 = Boolean.parseBoolean(splited[6]);

                Cell cell = grid.getCell(row,col);
                cell.setCellDrawMode(dm);
                cell.setRoaded(new boolean[]{d1,d2,d3,d4});
                cell.updateStyles();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
            e.printStackTrace();
        }
    }

    public class CountUpTask extends Task<Integer> {
        private int totalcount;
        private int count;
        public int example;

        public CountUpTask(int maxcount) {
            this.totalcount = maxcount;
            this.count = 0;
        }

        @Override
        public Integer call() throws StaleProxyException {

            while(count < totalcount) {
                count += 1;

                if( this.example == 1 ) {
                    Globals.timestepInterval = 2000;
                    Globals.trafficLightInterval = 2400;
                }
                world.example = this.example;
                world.runTimestep( );

                updateValue(count);

                try { Thread.sleep(Globals.timestepInterval); }
                catch (InterruptedException ex) { break; }
            }
            world.destroy();
            return count;
        }
    }

    CountUpTask countUpTask = new CountUpTask(200);


    void startWorldScene( ) throws StaleProxyException, InterruptedException {

        worldDrawer = new WorldDrawer(primStage, width, height, this);

        world = null;
        try {
            world = new TrafficLightWorld(grid, worldDrawer);
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }

        world.constructWorld( );
        worldDrawer.DrawWorld(world);

        ChangeListener<Integer> listener = new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable,
                                Integer oldValue,
                                Integer newValue) {

                worldDrawer.drawCars();
                worldDrawer.ReDrawTrafficLights();
                worldDrawer.currentTimestep = newValue;
            }
        };


        countUpTask.valueProperty().addListener( listener );

        countUpTask.example = 0;
        thread = new Thread(countUpTask);
        thread.start();
    }

    void startWorldSceneExample( ) throws StaleProxyException, InterruptedException {

        worldDrawer = new WorldDrawer(primStage, width, height, this);

        world = null;
        try {
            world = new TrafficLightWorld(grid, worldDrawer);
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }

        world.constructWorld( );
        worldDrawer.DrawWorld(world);

        ChangeListener<Integer> listener = new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable,
                                Integer oldValue,
                                Integer newValue) {

                worldDrawer.drawCars();
                worldDrawer.ReDrawTrafficLights();
                worldDrawer.currentTimestep = newValue;
            }
        };


        countUpTask.valueProperty().addListener( listener );
        countUpTask.example = 1;
        thread = new Thread(countUpTask);
        thread.start();
    }

    @Override
    public void stop() throws Exception {
        super.stop();

    }

    public static void main(String[] args) {
        launch(args);
    }

    public class Grid extends Pane {

        int rows;

        String class_names [] = {"cell-house", "cell-police", "cell-hospital", "cell-fire"};

        public int getRows() {
            return rows;
        }

        public void setRows(int rows) {
            this.rows = rows;
        }

        public int getColumns() {
            return columns;
        }

        void saveToFile( ) throws IOException {

            FileChooser fileChooser = new FileChooser();
            File myObj = fileChooser.showSaveDialog(primStage);

            FileWriter myWriter = new FileWriter(myObj);


            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < columns; col++) {
                    Cell cell = getCell(row,col);
                    myWriter.write(row + " " + col + " " + cell.cellDrawMode + " " + cell.getRoadedPrintable() + "\n");

                }
            }
            myWriter.close();
        }


        public void setColumns(int columns) {
            this.columns = columns;
        }

        public Cell[][] getCells() {
            return cells;
        }

        public void setCells(Cell[][] cells) {
            this.cells = cells;
        }

        int columns;

        double width;
        double height;

        Cell[][] cells;

        public Grid(int columns, int rows, double width, double height) {

            this.columns = columns;
            this.rows = rows;
            this.width = width;
            this.height = height;

            cells = new Cell[rows][columns];

        }

        /**
         * Add cell to array and to the UI.
         */
        public void add(Cell cell, int column, int row) {

            cells[row][column] = cell;

            double w = width / columns;
            double h = height / rows;
            double x = w * column;
            double y = h * row;

            cell.setLayoutX(x);
            cell.setLayoutY(y);
            cell.setPrefWidth(w);
            cell.setPrefHeight(h);

            getChildren().add(cell);

        }

        public Cell getCell( int row, int column) {
            return cells[row][column];
        }

        public boolean hasRoadLeft(int row, int column){
            if( column == 0 ){
                return false;
            } else {
                try {
                    if (row == rows) {
                        return cells[row - 1][column - 1].roaded[2];
                    } else {
                        return cells[row][column - 1].roaded[0];
                    }
                } catch (ArrayIndexOutOfBoundsException e ){

                    return false;
                }
            }
        }

        public boolean hasRoadRight(int row, int column){

            if( column >= columns - 2){
                return false;
            } else {
                try {
                    if( row == rows) {
                        return cells[row-1][column].roaded[2];
                    } else if( column == columns ){
                        return cells[row][column-1].roaded[0];
                    } else {
                        return cells[row][column].roaded[0];
                    }
                } catch (ArrayIndexOutOfBoundsException e){
                    return false;
                }
            }
        }

        public boolean hasRoadUp(int row, int column){
            if( row == 0 ){
                return false;
            } else {
                try {
                    if( row == rows) {
                        return cells[row - 2][column].roaded[3];
                    } else if( column == columns ){
                        return cells[row-1][column-1].roaded[1];
                    } else {
                        return cells[row - 1][column].roaded[3];
                    }
                } catch (ArrayIndexOutOfBoundsException e ){

                    return false;
                }
            }
        }

        public boolean hasRoadDown(int row, int column){
            if( row >= rows - 1){
                return false;
            } else {
                try {
                    if (row == rows) {
                        return cells[row - 1][column].roaded[3];
                    } else if( column == columns ){
                        return cells[row][column-1].roaded[1];
                    } else {
                        return cells[row][column].roaded[3];
                    }
                } catch (ArrayIndexOutOfBoundsException e ){

                    return false;
                }
            }
        }

        /**
         * Unhighlight all cells
         */
        public void unhighlight() {
            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < columns; col++) {
                    cells[row][col].unhighlight(null);
                }
            }
        }
    }

    public class Cell extends StackPane {

        int column;
        int row;
        Grid parent;

        public void setRoaded(boolean[] roaded) {
            this.roaded = roaded;
        }

        boolean roaded[] = new boolean[]{false,false,false,false};

        public DrawMode getCellDrawMode() {
            return cellDrawMode;
        }

        public void setCellDrawMode(DrawMode cellDrawMode) {
            this.cellDrawMode = cellDrawMode;
        }

        public DrawMode cellDrawMode = DrawMode.DRAW_NOTHING;

        public Cell(Grid parent, int column, int row) {

            this.column = column;
            this.row = row;
            this.parent = parent;

            getStyleClass().add("cell");

            setOpacity(0.9);
        }

        String getClassNameFromDrawingMode( )
        {
            switch (cellDrawMode){
                case DRAW_HOUSE:
                    return "cell-house";
                case DRAW_POLICESTATION:
                    return "cell-police";
                case DRAW_HOSPITAL:
                    return "cell-hospital";
                case DRAW_FIREHOUSE:
                    return "cell-fire";
                default:
                    return "cell";
            }
        }

        public void updateStyles( ){
            setStyle("-fx-border-color: " + getColorsStr() + "; -fx-border-width:  " + getWidthStr() + ";");
            for( String className : parent.class_names ) {
                getStyleClass().remove(className);
            }
            getStyleClass().add(getClassNameFromDrawingMode());
        }

        public void highlight(MouseEvent event) {
            cellDrawMode = drawMode;
            if( drawMode == DrawMode.DRAW_ROAD ) {
                double x = event.getX(), y = event.getY(), w = getPrefWidth(), h = getPrefHeight();
                setRoadValues(x,y,h,w,true);
                updateStyles();

            } else{
                for( String className : parent.class_names ) {
                    getStyleClass().remove(className);
                }
                getStyleClass().add(getClassNameFromDrawingMode());
            }
        }

        public void unhighlight(MouseEvent event) {

            if( drawMode == DrawMode.DRAW_ROAD && event != null) {
                double x = event.getX(), y = event.getY(), w = getPrefWidth(), h = getPrefHeight();
                setRoadValues(x,y,h,w,false);
                updateStyles();
            } else {
                cellDrawMode = DrawMode.DRAW_NOTHING;
                for( String className : parent.class_names ) {
                    getStyleClass().remove(className);
                }
            }
        }

        public void hoverHighlight() {
            // ensure the style is only once in the style list
            getStyleClass().remove("cell-hover-highlight");

            // add style
            getStyleClass().add("cell-hover-highlight");
        }

        public void hoverUnhighlight() {
            getStyleClass().remove("cell-hover-highlight");
        }

        public String toString() {
            return this.column + "/" + this.row;
        }


        public int argMin(double[] a) {
            double v = Double.MAX_VALUE;
            int ind = -1;
            for (int i = 0; i < a.length; i++) {
                if (a[i] < v) {
                    v = a[i];
                    ind = i;
                }
            }
            return ind;
        }

        void setRoadValues(double x, double y, double h, double w, boolean value) {
            double limits[] = {x, w - x, y, h - y};

            int argmin = argMin(limits);

            if (argmin == 0) {
                roaded[3] = value;
                if( column > 0 ){
                    Cell leftCell = parent.getCell(row, column-1 );
                    leftCell.roaded[1] = value;
                    leftCell.updateStyles();
                }
            }
            else if (argmin == 1) {
                roaded[1] = value;
                if( column < columns - 1 ){
                    Cell leftCell = parent.getCell(row, column+1 );
                    leftCell.roaded[3] = value;
                    leftCell.updateStyles();
                }
            }
            else if (argmin == 2) {
                roaded[0] = value;
                if( row > 0 ){
                    Cell leftCell = parent.getCell(row-1, column );
                    leftCell.roaded[2] = value;
                    leftCell.updateStyles();
                }
            }
            else if (argmin == 3) {
                roaded[2] = value;
                if( row < rows - 1 ){
                    Cell leftCell = parent.getCell(row+1, column );
                    leftCell.roaded[0] = value;
                    leftCell.updateStyles();
                }
            }
        }

        String getRoadedPrintable( ){

            String roadedStr = "";
            for( boolean r : roaded ){
                roadedStr += r + " ";
            }
            return roadedStr;
        }

        String getColorStrFromValue( boolean value ){

            if( value)
                return "red";
            else
                return "dodgerblue";
        }

        String getWidthStrFromValue( boolean value ){

            if( value)
                return "3px";
            else
                return "1px";
        }

        String getColorsStr( ) {

            String colorStr = "";

            for (int i = 0; i < 4; i++) {

                colorStr += getColorStrFromValue(roaded[i]) + " ";
            }
            return colorStr;
        }

        String getWidthStr( ) {

            String colorStr = "";

            for (int i = 0; i < 4; i++) {

                colorStr += getWidthStrFromValue(roaded[i]) + " ";
            }
            return colorStr;
        }
    }

    public class MouseGestures {

        public void makePaintable(Node node) {


            // that's all there is needed for hovering, the other code is just for painting
            if (showHoverCursor) {
                node.hoverProperty().addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

                        if (newValue) {
                            ((Cell) node).hoverHighlight();
                        } else {
                            ((Cell) node).hoverUnhighlight();
                        }

                    }

                });
            }

            node.setOnMousePressed(onMousePressedEventHandler);
            node.setOnDragDetected(onDragDetectedEventHandler);
            node.setOnMouseDragEntered(onMouseDragEnteredEventHandler);

        }

        EventHandler<MouseEvent> onMousePressedEventHandler = event -> {

            Cell cell = (Cell) event.getSource();

            if (event.isPrimaryButtonDown()) {
                cell.highlight(event);

            } else if (event.isSecondaryButtonDown()) {
                cell.unhighlight(event);
            }
        };

        EventHandler<MouseEvent> onMouseDraggedEventHandler = event -> {

            PickResult pickResult = event.getPickResult();
            Node node = pickResult.getIntersectedNode();

            if (node instanceof Cell) {

                Cell cell = (Cell) node;

                if (event.isPrimaryButtonDown()) {
                    cell.highlight(event);
                } else if (event.isSecondaryButtonDown()) {
                    cell.unhighlight(event);
                }

            }

        };

        EventHandler<MouseEvent> onMouseReleasedEventHandler = event -> {
        };

        EventHandler<MouseEvent> onDragDetectedEventHandler = event -> {

            Cell cell = (Cell) event.getSource();
            cell.startFullDrag();

        };

        EventHandler<MouseEvent> onMouseDragEnteredEventHandler = event -> {

            Cell cell = (Cell) event.getSource();

            if (event.isPrimaryButtonDown()) {
                cell.highlight(event);
            } else if (event.isSecondaryButtonDown()) {
                cell.unhighlight(event);
            }


        };

    }
}