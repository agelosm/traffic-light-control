package trafficLightPackage;

import jade.core.AID;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;

enum LightColor{
    GREEN,
    RED
};

public class TrafficLight {
    /**
     * Class that represents a traffic light, and spawns it's corresponding agent,
     * which can receive emergency switches from the controller
     */

    public LightColor color = LightColor.RED;
    public ArrayList<Direction> openDirections = new ArrayList<Direction>();

    Node node;
    int row, col;
    public AID aid;
    WorldDrawer worldDrawer;
    ArrayList<AgentController> agentControllers = new ArrayList<>();
    ArrayList<Car> waitingCars;
    int interval = Globals.trafficLightInterval;
    int emergencyInterval = Globals.trafficLightEmergencyInterval;
    boolean onEmergency = false;

    public void terminate( ){
        for (AgentController agentController : agentControllers ){
            try {
                agentController.kill();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
    }

    public TrafficLight(WorldDrawer worldDrawer, AID cid, LightColor color, Node node, int row, int col, AgentContainer container) throws StaleProxyException {
        this.color = color;
        this.node = node;
        this.row = row;
        this.col = col;
        openDirections.add(Direction.UP);
        openDirections.add(Direction.DOWN);
        this.openDirections = openDirections;
        this.worldDrawer = worldDrawer;

        Object[] args = {node, worldDrawer, this};

        AgentController agent = container.createNewAgent("TrafficLight:" +row+":"+col,
                "trafficLightPackage.TrafficLightAgent", args);
        agentControllers.add(agent);

        aid = new AID("TrafficLight:" +row+":"+col, false);

        agent.start();
    }

    void addWaitingCar( Car car )
    {
        waitingCars.add(car);
    }

    public void setColor(LightColor color) {

        if( this.color == LightColor.RED && color == LightColor.GREEN ){

            waitingCars.clear();
        }
        this.color = color;
    }

    private void swap( )
    {
        if( openDirections.contains(Direction.UP) ){
            openDirections.remove(Direction.UP);
            openDirections.remove(Direction.DOWN);
            openDirections.add(Direction.LEFT);
            openDirections.add(Direction.RIGHT);
        } else {
            openDirections.remove(Direction.RIGHT);
            openDirections.remove(Direction.LEFT);
            openDirections.add(Direction.UP);
            openDirections.add(Direction.DOWN);
        }
    }

    public void setOnEmergency( Direction direction ){

            emergencyInterval = Globals.trafficLightEmergencyInterval;
            onEmergency = true;
            if (!openDirections.contains(direction)) {
                swap();
            }
    }

    public void update() {

        if( !onEmergency ){
            interval--;
            if( interval == 0 ){
                swap();
                interval = Globals.trafficLightInterval;
            }
        } else {
            emergencyInterval--;
            if( emergencyInterval == 0 ){
                onEmergency = false;
                interval = Globals.trafficLightInterval;
                swap();
            }
        }
    }
}
