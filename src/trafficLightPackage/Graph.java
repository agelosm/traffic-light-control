package trafficLightPackage;

import java.util.*;

public class Graph {
    /**
     * This class is a graph representation of the city grid, and is used for path calculations
     */

    private Set<Node> nodes = new HashSet<>();

    public void addNode(Node nodeA) {
        nodes.add(nodeA);
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public void setNodes(Set<Node> nodes) {
        this.nodes = nodes;
    }

    public void BellmanFord(Node startNode)
    {
        int V = nodes.size();

        int E = 0;
        for( Node n : nodes ){

            E += n.adjacentNodes.size();
            n.setDistance(Integer.MAX_VALUE);
            n.shortestPath.clear();
        }
        startNode.setDistance(0);


        for (int i = 1; i <= V - 1; i++) {
            for (Node src : nodes) {
                for( Node dst : src.getAdjacentNodes().keySet() ) {
                    if (src.getDistance() != Integer.MAX_VALUE && src.getDistance() + 1 < dst.getDistance() ) {
                        dst.setDistance( src.getDistance() + 1 );
                        dst.shortestPath = (ArrayList<Node>) src.shortestPath.clone();
                        dst.shortestPath.add(0,src);
                    }
                }
            }
        }
    }

}

