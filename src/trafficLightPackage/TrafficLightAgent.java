package trafficLightPackage;

import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TrafficLightAgent extends Agent {
    /**
     * This agent awaits for a message from the controller, in order to change the light color
     */

    Node node;
    TrafficLight trafficLight;
    private Codec codec = new LEAPCodec();
    private Ontology ontology = TrafficLightRequestOntology.getInstance();
    WorldDrawer worldDrawer;
    AID cid;
    int interval = 7;

    @Override
    protected void setup() {
        super.setup();

        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        Object[] args = getArguments();
        node = (Node)args[0];
        cid = new AID("TrafficController", false);
        worldDrawer = (WorldDrawer)args[1];
        trafficLight = (TrafficLight) args[2];

        addBehaviour(new CyclicBehaviour(this) {
            public void action() {

                TrafficLightRequest request = receiveMessage();

                if (request != null) {

                    String messageDirection = (String) request.getDirection().get(0);

                    trafficLight.setOnEmergency(Direction.valueOf(messageDirection));
                }
            }
        });
    }


    TrafficLightRequest receiveMessage( )
    {
        ACLMessage inputMessage;
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchLanguage(codec.getName()),
                MessageTemplate.MatchOntology(ontology.getName()) );

        inputMessage = blockingReceive(mt);

        if (inputMessage != null) {

            ContentElement ce = null;
            try {
                ce = getContentManager().extractContent(inputMessage);
            } catch (Codec.CodecException e) {
                e.printStackTrace();
            } catch (OntologyException e) {
                e.printStackTrace();
            }
            if (ce instanceof TrafficLightRequest) {
                TrafficLightRequest trafficLightRequest = (TrafficLightRequest) ce;
                return trafficLightRequest;
            }
        }
        return null;
    }
}
