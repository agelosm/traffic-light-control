package trafficLightPackage;

import jade.core.AID;

import java.util.ArrayList;

public class PrivateCar extends Car{
    /**
     *  A class that represents a private car, with it's corresponding emergency priority
     *
     *  Private car overrides the MoveForward method, since they don't contact the traffic controller during their
     *  interaction with the traffic lights, in contrast with all the emergency vehicles
     */

    protected void setup() {
        super.setup();
        init(dto.Cid, dto.path, dto.trafficLights, dto.place, dto.emergency);
    }

    protected void init(AID controller, ArrayList<Node> path, ArrayList<TrafficLight> trafficlights, Node place, boolean onemergency){
        super.init(controller, path, trafficlights, place, onemergency);
        this.emergencyPriority = 2;
    }

    @Override
    void MoveForward(){

        TrafficLight trafficLight = isTrafficLightOnNode(place);
        if( trafficLight != null ){

            nextDirection = getNextDirection();
            if( trafficLight.openDirections.contains(nextDirection) ){

                advance();
            }
        } else {
            advance();
        }
    }
}
