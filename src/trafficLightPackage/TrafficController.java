package trafficLightPackage;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.leap.LEAPCodec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.HashMap;

public class TrafficController extends Agent {
    /**
     * The class of the traffic controller agent
     */

    ArrayList<TrafficLight> trafficLights;
    WorldDrawer worldDrawer;
    private Codec codec = new LEAPCodec();
    private Ontology ontology = TrafficLightRequestOntology.getInstance();
    AID receivedId;

    HashMap<TrafficLight, Integer> weightHashMap = new HashMap<TrafficLight, Integer>();

    protected void takeDown() {
        super.takeDown();

        // This agent will take care to shut the Jade system upon termination
        Codec codec = new SLCodec();
        Ontology jmo = JADEManagementOntology.getInstance();
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(jmo);
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(getAMS());
        msg.setLanguage(codec.getName());
        msg.setOntology(jmo.getName());
        try {
            getContentManager().fillContent(msg, new Action(getAID(), new ShutdownPlatform()));
            send(msg);
        }
        catch (Exception e) {}
    }

    @Override
    protected void setup() {

        Object[] args = getArguments();

        trafficLights = (ArrayList<TrafficLight>) args[0];
        worldDrawer = (WorldDrawer) args[1];

        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        // The controller runs in a cyclic message-receiving behaviour
        addBehaviour(new CyclicBehaviour(this) {
            public void action() {

                TrafficLightRequest request = receiveMessage();

                if (request != null) {

                    if( checkMessageValid(request) ) {

                        processRequest(request);
                    }
                }
            }
        });
    }

    boolean checkMessageValid( TrafficLightRequest request ){

        String senderName = receivedId.getLocalName();

        String validNames[] = {"ambulance", "police", "firetruck"};

        for( String validName : validNames ){

            if( senderName.contains(validName)){
                return true;
            }
        }
        return false;
    }

    void processRequest(TrafficLightRequest request)
    {
        int x = request.getRow();
        int y = request.getColumn();
        int weight = (int) request.getEmergencyPriority().get(0);

        for( TrafficLight trafficLight : trafficLights ){

            if( trafficLight.row == x && trafficLight.col == y ){

                /* If no one of higher emergency has already binded the traffic light, forward the request */
                if( !trafficLight.onEmergency ||
                        ( weightHashMap.containsKey(trafficLight) && weightHashMap.get(trafficLight) < weight ) ){

                    weightHashMap.put(trafficLight, weight);
                    SendMessage(request,trafficLight.aid);
                }
            }
        }
    }

    /* Receive a message from a car-agent using the TrafficLightRequest ontology */
    TrafficLightRequest receiveMessage() {
        ACLMessage inputMessage;
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchLanguage(codec.getName()),
                MessageTemplate.MatchOntology(ontology.getName()));

        inputMessage = blockingReceive(mt);

        if (inputMessage != null) {

            ContentElement ce = null;
            try {
                ce = getContentManager().extractContent(inputMessage);
                receivedId = inputMessage.getSender();
            } catch (Codec.CodecException e) {
                e.printStackTrace();
            } catch (OntologyException e) {
                e.printStackTrace();
            }
            if (ce instanceof TrafficLightRequest) {
                TrafficLightRequest trafficLightRequest = (TrafficLightRequest) ce;
                return trafficLightRequest;
            }
        }
        return null;
    }

    jade.util.leap.ArrayList initList( Object o ){

        jade.util.leap.ArrayList list = new jade.util.leap.ArrayList();
        list.add(o);
        return list;
    }

    void SendMessage(TrafficLightRequest request, AID aid){

        try {
            worldDrawer.addMessage(aid, request);
            send(request.toACLMessage(getContentManager(),aid));
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}
