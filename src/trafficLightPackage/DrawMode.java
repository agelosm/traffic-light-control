package trafficLightPackage;

public enum DrawMode {
    DRAW_NOTHING,
    DRAW_ROAD,
    DRAW_HOUSE,
    DRAW_HOSPITAL,
    DRAW_POLICESTATION,
    DRAW_FIREHOUSE
}
