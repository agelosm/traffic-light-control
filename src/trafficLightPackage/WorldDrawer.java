package trafficLightPackage;

import jade.core.AID;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class WorldDrawer{

    /**
     * This class handles all the drawing, including the statistics drawing
     */

    public Integer currentTimestep = 0;
    Stage primStage;
    private double width, height;
    private TrafficLightWorld world;

    int pointDist = 50;
    int marginLeft = 50, marginTop = 50;
    private Canvas canvasTrafficLight, canvasCars;
    private GraphicsContext gcTrafficLight, gcCars;
    ReentrantLock lock = new ReentrantLock();

    Main parent;

    StatisticsManager statisticsManager = new StatisticsManager();

    ConcurrentHashMap<AID, CarDTO> carPositions = new ConcurrentHashMap<>();
    private Group root, lightsGroup, carsGroup;
    private TableView<ListedMessage> listView;
    private TableView<ListedAgent> agentListview;
    private TableView<StatisticListed> statView;
    private BarChart<String, Number> bc;

    public WorldDrawer(Stage primStage, double width, double height, Main main) {

        this.primStage = primStage;
        this.width = width;
        this.height = height;
        this.parent = main;
    }

    File getFileFromBuildingType( BuildingType buildingType)
    {
        switch (buildingType) {

            case HOUSING:
                return new File("images/house.png");

            case HOSPITAL:
                return new File("images/hospital.png");

            case POLICE_STATION:
                return new File("images/police.png");

            case FIREHOUSE:
                return new File("images/fire.png");

        }
        return new File("images/house.png");
    }

    private WritableImage createCircledNumber(int x, int y) {
        StackPane sPane = new StackPane();
        sPane.setPrefSize(pointDist, pointDist);

        Circle c = new Circle(pointDist/2.0);
        c.setStroke(Color.BLACK);
        c.setFill(Color.TRANSPARENT);
        c.setStrokeWidth(2);
        sPane.getChildren().add(c);

        Text txtNum = new Text(x+","+y);
        sPane.getChildren().add(txtNum);
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        return sPane.snapshot(parameters, null);
    }

    void getCarPosition( AID aid, CarDTO carDTO)
    {
        carPositions.put(aid, carDTO);
    }

    void removeCarPosition( AID aid )
    {
        carPositions.remove(aid);
    }

    String getCarImageFromCarType( AID aid ){

        if( aid.getLocalName().contains("olice") ){
            return "images/police_car.png";
        } else if( aid.getLocalName().contains("rivate") ){
            int intValue = Integer.parseInt(aid.getLocalName().replaceAll("[^0-9]", "")) % 6 + 1;

            return "images/ix" + intValue +".png";
        } else if( aid.getLocalName().contains("bulance") ) {
            return "images/ambulance.png";
        } else if( aid.getLocalName().contains("fire")){
            return "images/firetruck.png";
        }
        return "images/ix1.png";
    }

    void drawCars( )
    {
        agentListview.getItems().clear();
        carsGroup.getChildren().clear();
        statisticsManager.resetAgents();
        int carOffset = (int) ((int)pointDist/2.5);
        int carConflict = 7;
        int conflictId = 0;

        HashMap<Node, Integer> positionMap = new HashMap<>();

        Iterator it = carPositions.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();

            AID carAid = (AID) pair.getKey();
            CarDTO carDTO = (CarDTO) pair.getValue();
            Node carNode = carDTO.place;
            Direction direction = carDTO.direction;
            Direction nextDirection = carDTO.nextDirection;

            if( positionMap.containsKey(carNode)){

                conflictId = positionMap.get(carNode);
                positionMap.remove(carNode);
                positionMap.put( carNode, conflictId + 1 );

            } else {
                conflictId = 0;
                positionMap.put(carNode, 1);

            }

            int topLeftX =  carNode.col*pointDist + marginLeft + pointDist/4;
            int topLeftY = carNode.row*pointDist + marginTop + pointDist/4;
            File file = new File(getCarImageFromCarType(carAid));
            Image image = new Image(file.toURI().toString(), 25, 35, false, false);
            ImageView view = new ImageView(image);

            addAgent( carAid, carDTO);

            if (direction == Direction.UP) {
                view.setRotate(0);
                topLeftX += carOffset;
                topLeftY -= conflictId*carConflict - carOffset;
                if( nextDirection == Direction.RIGHT || nextDirection == Direction.LEFT ) {
                    topLeftY += carOffset;
                }
            } else if (direction == Direction.DOWN) {
                view.setRotate(180);
                topLeftX -= carOffset;
                topLeftY += conflictId*carConflict + carOffset;
                if( nextDirection == Direction.RIGHT || nextDirection == Direction.LEFT ) {
                    topLeftY -= carOffset;
                }
            } else if (direction == Direction.LEFT) {
                view.setRotate(270);
                topLeftY -= carOffset;
                topLeftX += conflictId*carConflict;
                if( nextDirection == Direction.UP || nextDirection == Direction.DOWN ) {
                    topLeftX += carOffset;
                }
            } else {
                view.setRotate(90);
                topLeftY += carOffset*0.7;
                topLeftX -= conflictId*carConflict;
                if( nextDirection == Direction.UP || nextDirection == Direction.DOWN ) {
                    topLeftX -= carOffset;
                }
            }

            view.setX(topLeftX);
            view.setY(topLeftY);

            carsGroup.getChildren().add(view);
            if( carAid.getLocalName().contains("olice") ){
                statisticsManager.currentPoliceCars++;
            } else if( carAid.getLocalName().contains("rivate")){
                statisticsManager.currentPrivateCars++;
            } else if( carAid.getLocalName().contains("bulance")){
                statisticsManager.currentAmbulances++;
            } else if( carAid.getLocalName().contains("ire")){
                statisticsManager.currentFiretrucks++;
            }


        }
        statisticsManager.update();
    }

    void drawBuildings( GraphicsContext gc )
    {
        for( Building building : world.city.buildings ){

                int topLeftX =  building.nodes.get(0).col*pointDist + marginLeft;
                int topLeftY = building.nodes.get(0).row*pointDist + marginTop;

                File file = getFileFromBuildingType(building.type);
                Image image = new Image(file.toURI().toString(), 1.3*pointDist, 1.3*pointDist, false, false);
                gc.drawImage(image, topLeftX , topLeftY);
        }
    }

    void drawTrafficLights( GraphicsContext gc )
    {
        lightsGroup.getChildren().clear();
        for( TrafficLight trafficLight : world.city.trafficLights ){
            int topLeftX =  trafficLight.node.col*pointDist + marginLeft;
            int topLeftY = trafficLight.node.row*pointDist + marginTop;

            String filepath = (trafficLight.openDirections.contains(Direction.RIGHT) || trafficLight.openDirections.contains(Direction.LEFT) )
                    ? "images/left_and_right_light.png" : "images/up_and_down_light.png";
            File file = new File(filepath);
            Image image = new Image(file.toURI().toString(), pointDist, pointDist, false, false);
            ImageView view = new ImageView(image);
            view.setX(topLeftX);
            view.setY(topLeftY);
            view.setOpacity(0.5);

            if(trafficLight.onEmergency ){
                File file2 = new File("images/conflict.png");
                Image image2 = new Image(file2.toURI().toString(), pointDist, pointDist, false, false);
                ImageView view2 = new ImageView(image2);
                view2.setX(topLeftX-pointDist*0.7);
                view2.setY(topLeftY-pointDist*0.7);

                lightsGroup.getChildren().add(view2);
            }

            lightsGroup.getChildren().add(view);
        }
    }

    private void drawNodesEdges(GraphicsContext gcNodes, GraphicsContext gcEdges, GraphicsContext gcSplitEdges ) {
        gcNodes.setFill(Color.GREEN);
        gcEdges.setStroke(Color.GRAY);
        gcEdges.setLineWidth(55);
        gcSplitEdges.setStroke(Color.WHITE);
        gcSplitEdges.setLineWidth(1);
        gcSplitEdges.setLineDashes(4);

        for (int i = 0; i < world.city.rows; i++) {
            for (int j = 0; j < world.city.columns; j++) {

                int topLeftX =  j*pointDist + marginLeft;
                int topLeftY = i*pointDist + marginTop;

                gcNodes.drawImage(createCircledNumber(i,j), topLeftX, topLeftY);

                int centerX = topLeftX + pointDist/2;
                int centerY = topLeftY + pointDist/2;

                Node node = world.city.grid[i][j];
                if( node.left() ){
                    gcEdges.strokeLine(centerX, centerY, centerX - pointDist, centerY);
                    gcSplitEdges.strokeLine(centerX, centerY, centerX - pointDist, centerY);
                }
                if( node.right()  ){
                    gcEdges.strokeLine(centerX, centerY, centerX + pointDist, centerY);
                    gcSplitEdges.strokeLine(centerX, centerY, centerX + pointDist, centerY);
                }
                if( node.up()){
                    //gcEdges.strokeLine(centerX, centerY, centerX, centerY - pointDist);
                   // gcSplitEdges.strokeLine(centerX, centerY, centerX, centerY - pointDist);
                }
                if( node.down()){
                    gcEdges.strokeLine(centerX, centerY, centerX, centerY + pointDist);
                    gcSplitEdges.strokeLine(centerX, centerY, centerX, centerY + pointDist);
                }

            }

        }
    }

    void ReDrawTrafficLights( )
    {
        gcTrafficLight.clearRect(0, 0, canvasTrafficLight.getWidth(), canvasTrafficLight.getHeight());
        drawTrafficLights(gcTrafficLight);
        canvasTrafficLight.toFront();

        primStage.show();
    }

    public class ListedMessage {

        private String sender, receiver, msg, emW, color, time;

        public String getSender() {
            return sender;
        }

        public String getTime(){ return time; }
        public void setTime( String time ){ this.time = time; }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getReceiver() {
            return receiver;
        }

        public void setReceiver(String receiver) {
            this.receiver = receiver;
        }

        public String getEmW() {
            return emW;
        }

        public void setEmW(String emW) {
            this.emW = emW;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String onEm) {
            this.color = onEm;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public ListedMessage(){}

        public ListedMessage(AID receiverId, TrafficLightRequest message) {
            this.sender = ( (AID)message.getSenders().get(0) ).getLocalName();
            this.receiver = receiverId.getLocalName();
            this.emW = Integer.toString((int)message.getEmergencyPriority().get(0));
            this.color = (boolean)message.getColor().get(0) ? "Red" : "Green";
            this.msg = message.getRow() + "," + message.getColumn();
            this.time = currentTimestep.toString();
        }
    };

    public class ListedAgent {

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getCurrent() {
            return current;
        }

        public void setCurrent(String current) {
            this.current = current;
        }

        private String type, weight, start, end, current;

        public ListedAgent(){}

        public ListedAgent(AID carId, CarDTO carDTO) {

            this.current = carDTO.place.getName();
            this.start = carDTO.start.getName();
            this.end = carDTO.end.getName();

            this.type = carId.getLocalName();

            statisticsManager.currentAgents++;

            if( type.contains("olice") ){
                this.weight = Integer.toString(carDTO.emergency ? 3 : 1);
            } else if( type.contains("rivate")){
                this.weight = Integer.toString(carDTO.emergency ? 2 : 1);
            } else if( type.contains("bulance")){
                this.weight = Integer.toString(carDTO.emergency ? 5 : 1);
            } else if( type.contains("ire")){
                this.weight = Integer.toString(carDTO.emergency ? 4 : 1);
            }
        }
    };

    public class StatisticListed{

        public String property, value;

        public StatisticListed( ){};

        public StatisticListed( String prop, String val)
        {
            property = prop;
            value = val;
        };

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    };

    class StatisticsManager{

        XYChart.Series series1, series2, series3, series4;

        public int totalMessages = 0;
        public int currentAgents = 0;
        public int totalPoliceCars = 0, currentPoliceCars = 0;
        public int totalPrivateCars = 0, currentPrivateCars = 0;
        public int totalAmbulances = 0, currentAmbulances = 0;
        public int totalFiretrucks = 0, currentFiretrucks = 0;

        public void resetAgents( ){
            currentAmbulances = currentFiretrucks = currentPoliceCars = currentPrivateCars = currentAgents = 0;
        }

        public BarChart<String, Number> createBarChart( )
        {

            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();

            BarChart<String, Number> bcc = new BarChart<String, Number>(xAxis, yAxis);
            bcc.setTitle("Total agents per type");
            bcc.setBarGap(40);
            xAxis.setLabel("Type");
            yAxis.setLabel("Count");

            series1 = new XYChart.Series();
            series1.setName("Private");
            series1.getData().add(new XYChart.Data("", statisticsManager.totalPrivateCars));

            series2 = new XYChart.Series();
            series2.setName("Police");
            series2.getData().add(new XYChart.Data("", statisticsManager.totalPoliceCars));

            series3 = new XYChart.Series();
            series3.setName("Ambulance");
            series3.getData().add(new XYChart.Data("", statisticsManager.totalAmbulances));

            series4 = new XYChart.Series();
            series4.setName("Firetruck");
            series4.getData().add(new XYChart.Data("", statisticsManager.totalFiretrucks));

            bcc.getData().addAll(series1, series2, series3, series4);

            return bcc;
        }

        public void update( ){

            statView.getItems().clear();
            statView.getItems().add(new StatisticListed("Total Messages", Integer.toString(totalMessages)));
            statView.getItems().add(new StatisticListed("Current Agents", Integer.toString(currentAgents)));
            statView.getItems().add(new StatisticListed("Police Cars", Integer.toString(currentPoliceCars)));
            statView.getItems().add(new StatisticListed("Ambulances", Integer.toString(currentAmbulances)));
            statView.getItems().add(new StatisticListed("Firetrucks", Integer.toString(currentFiretrucks)));
            statView.getItems().add(new StatisticListed("Private Cars", Integer.toString(currentPrivateCars)));
            statView.refresh();


            series1.getData().clear();
            series1.getData().add(new XYChart.Data("", statisticsManager.totalPrivateCars));

            series2.getData().clear();
            series2.getData().add(new XYChart.Data("", statisticsManager.totalPoliceCars));

            series3.getData().clear();
            series3.getData().add(new XYChart.Data("", statisticsManager.totalAmbulances));

            series4.getData().clear();
            series4.getData().add(new XYChart.Data("", statisticsManager.totalFiretrucks));
            bc.setAnimated(false);
        }
    };

    void DrawWorld(TrafficLightWorld world){
        listView = new TableView<>();

        TableColumn<ListedMessage, String> timeColumn = new TableColumn<>("Time");
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));

        TableColumn<ListedMessage, String> senderColumn = new TableColumn<>("Sender");
        senderColumn.setCellValueFactory(new PropertyValueFactory<>("sender"));

        TableColumn<ListedMessage, String> recColumn = new TableColumn<>("Receiver");
        recColumn.setCellValueFactory(new PropertyValueFactory<>("receiver"));

        TableColumn<ListedMessage, String> msgColumn = new TableColumn<>("Coords");
        msgColumn.setCellValueFactory(new PropertyValueFactory<>("msg"));

        TableColumn<ListedMessage, String> wColumn = new TableColumn<>("Weight");
        wColumn.setCellValueFactory(new PropertyValueFactory<>("emW"));

        TableColumn<ListedMessage, String> emColumn = new TableColumn<>("Color");
        emColumn.setCellValueFactory(new PropertyValueFactory<>("color"));

        listView.getColumns().addAll(timeColumn, senderColumn, recColumn, msgColumn, emColumn,  wColumn);

        agentListview = new TableView<>();

        TableColumn<ListedAgent, String> typeColumn = new TableColumn<>("Name");
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));

        TableColumn<ListedAgent, String> startColumn = new TableColumn<>("Start");
        startColumn.setCellValueFactory(new PropertyValueFactory<>("start"));

        TableColumn<ListedAgent, String> endColumn = new TableColumn<>("Goal");
        endColumn.setCellValueFactory(new PropertyValueFactory<>("end"));

        TableColumn<ListedAgent, String> currentColumn = new TableColumn<>("Current");
        currentColumn.setCellValueFactory(new PropertyValueFactory<>("current"));

        TableColumn<ListedAgent, String> weightColumn = new TableColumn<>("Weight");
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));

        agentListview.getColumns().addAll(typeColumn, startColumn, endColumn, currentColumn, weightColumn);

        statView = new TableView<>();

        TableColumn<StatisticListed, String> propColumn = new TableColumn<>("Property");
        propColumn.setCellValueFactory(new PropertyValueFactory<>("property"));

        TableColumn<StatisticListed, String> valColumn = new TableColumn<>("Value");
        valColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        statView.getColumns().addAll(propColumn, valColumn);

        this.world = world;
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int mainCanvasHeight = dimension.height;
        int mainCanvasWidth = dimension.width - 750;


        primStage.setTitle("Drawing Operations Test");
        root = new Group();
        lightsGroup = new Group();
        carsGroup = new Group();
        //Canvas canvas = new Canvas(width, height);
        Canvas canvas = new Canvas(mainCanvasWidth, mainCanvasHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        BorderPane borderPane = new BorderPane();
        //borderPane.setTop(cb);

        Canvas canvas2 = new Canvas(mainCanvasWidth, mainCanvasHeight);
        GraphicsContext gc2 = canvas2.getGraphicsContext2D();

        Canvas canvas3 = new Canvas(mainCanvasWidth, mainCanvasHeight);
        GraphicsContext gc3 = canvas3.getGraphicsContext2D();

        canvasTrafficLight = new Canvas(mainCanvasWidth, mainCanvasHeight);
        gcTrafficLight = canvasTrafficLight.getGraphicsContext2D();

        canvasCars = new Canvas(mainCanvasWidth, mainCanvasHeight);
        gcCars = canvasCars.getGraphicsContext2D();

        drawNodesEdges(gc, gc2, gc3);
        drawBuildings(gc3);
        drawTrafficLights(gcTrafficLight);
        Pane pane = new Pane();
        //pane.getChildren().add(canvas);

        pane.getChildren().addAll(canvas2, canvas3, canvasTrafficLight, carsGroup, lightsGroup);

        canvasTrafficLight.toFront();
        canvasCars.toFront();
        borderPane.setCenter(pane);
        borderPane.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        pane.setStyle("-fx-border-color: black");

        TabPane tabPane = new TabPane();
        tabPane.prefWidth(400);

        Tab tab2 = new Tab("Agents");
        Tab tab1 = new Tab("Messages");
        Tab tab3 = new Tab("Statistics");

        tabPane.getTabs().add(tab2);
        tabPane.getTabs().add(tab1);
        tabPane.getTabs().add(tab3);

        VBox vBox = new VBox(tabPane);

        HBox hbox = new HBox(listView);
        hbox.setPrefWidth(500);
        listView.setPrefWidth(500);
        hbox.getStyleClass().add("hbox");
        hbox.setStyle("-fx-padding: 10px;");
        Pane pane2 = new Pane();
        pane2.setPrefWidth(500);



        bc = statisticsManager.createBarChart();

        vBox.getChildren().add(bc);

        tab1.setContent(hbox);

        tab2.setContent(agentListview);

        tab3.setContent(statView);

        borderPane.setRight(pane2);

        pane2.getChildren().add(vBox);

        root.getChildren().add(borderPane);

        primStage.setScene(new Scene(root));
        primStage.show();
    }

    void addMessage( AID rid, TrafficLightRequest trafficLightRequest ){

        lock.lock();
        statisticsManager.totalMessages++;
        listView.getItems().add(new ListedMessage(rid, trafficLightRequest));
        listView.refresh();
        lock.unlock();
    }

    void addAgent( AID rid, CarDTO carDTO ){

        agentListview.getItems().add(new ListedAgent(rid, carDTO));
        agentListview.refresh();

    }
}
